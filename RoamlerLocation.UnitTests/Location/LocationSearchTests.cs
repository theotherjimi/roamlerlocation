﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using RoamlerLocation.Api.Location;
using RoamlerLocation.Api.Models;
using RoamlerLocation.Api.Repositories;

namespace RoamlerLocation.UnitTests.Location;

[TestFixture]
public class LocationSearchTests
{
    private LocationSearch _locationSearch = null!;

    [SetUp]
    public void Setup()
    {
        var mockLocationRepository = new Mock<ILocationRepository>();
        mockLocationRepository.Setup(m => m.GetLocationData())
            .Returns(new List<Api.Models.Location>
            {
                new("Far Away 1", 0, 0),
                new("Close 1", 52.40044, 4.69321),
                new("Close 2", 52.60044, 4.79321),
                new("Close 3", 52.20044, 4.89321),
                new("Close 4", 52.10044, 4.99321),
                new("Far Away 2", 1, 40)
            });
        _locationSearch = new LocationSearch(mockLocationRepository.Object);
    }
    
    [TestCase(
        52.40044,
        4.89321,
        5,
        100000,
        new[] {52.40044, 52.20044, 52.60044, 52.10044},
        new[] {4.69321, 4.89321, 4.79321, 4.99321})]
    public void LocationSearch_ReturnResults(double latitude,
        double longitude,
        int maxResults,
        int maxDistance,
        double[] expectedLatitudes,
        double[] expectedLongitudes)
    {
        // arrange
        var targetLocation = new AnonymousLocation(latitude, longitude);
        var expectedLocations = expectedLatitudes
            .Zip(expectedLongitudes)
            .Select(p => new AnonymousLocation(p.First, p.Second));
        // act
        var actual = _locationSearch.Search(targetLocation, maxDistance, maxResults);
        // assert
        CollectionAssert.AreEqual(expectedLocations, actual.Select(x => new AnonymousLocation(x.Latitude, x.Longitude)), new TestLocationComparer());
    }

    private class TestLocationComparer : IComparer
    {
        public int Compare(object? a, object? b)
        {
            var x = a as AnonymousLocation;
            var y = b as  AnonymousLocation;
            if (Math.Abs(x!.Latitude - y!.Latitude) < 0.01 && Math.Abs(x.Longitude - y.Longitude) < 0.01)
            {
                return 0;
            }

            return x.Latitude.CompareTo(y.Latitude);
        }
    }
}