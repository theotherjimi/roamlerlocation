### Roamler Assignment

I've chosen to implement a small web api that allows a user to search for locations within a certain distance from a target

The locations are loaded from a CSV file on the first request and then cached in memory to make future requests faster

In order to keep the speed of the location search quick, the searching strategy is as follows:

1. Filter the location list to only include locations within the max distance specified
2. Order the locations by distance to the target - with the closest locations first
3. Limit the number of results to the number requested by the user

While this strategy is quick for the number of locations in the sample data it will become slower as the total number of locations grows.
There are number of solutions to this. The simplest to implement would likely be to group the locations and only load the locations that are in a group that is within a certain distance from the target.
However this is beyond the scope of this assignment

In order to keep the code readable, and to make it easier to test I've separated the various components of the solution.
Each component relies only upon an abstraction - this allows for mocking dependencies in unit tests. You can see an example unit test in the solution.
While the unit tests are not comprehensive it does demonstrate the testability of the solution