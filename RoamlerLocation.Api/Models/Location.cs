﻿namespace RoamlerLocation.Api.Models;

public class Location : AnonymousLocation
{
    public string Address { get; }

    public Location(string address, double latitude, double longitude) : base(latitude, longitude)
    {
        Address = address;
    }
}