﻿namespace RoamlerLocation.Api.Models;

public class LocationResult : Location
{
    public double Distance { get; set; }

    public LocationResult(string address, double latitude, double longitude, double distance) : base(address, latitude, longitude)
    {
        Distance = distance;
    }
}