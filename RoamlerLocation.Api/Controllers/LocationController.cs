using Microsoft.AspNetCore.Mvc;
using RoamlerLocation.Api.Location;
using RoamlerLocation.Api.Models;

namespace RoamlerLocation.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class LocationController : ControllerBase
{
    private readonly LocationSearch _locationSearch;

    public LocationController(LocationSearch locationSearch)
    {
        _locationSearch = locationSearch;
    }

    [HttpGet]
    public IEnumerable<LocationResult> Search(double latitude, double longitude, int maxDistance, int maxResults)
    {
        return _locationSearch.Search(new AnonymousLocation(latitude, longitude), maxDistance, maxResults);
    }
}