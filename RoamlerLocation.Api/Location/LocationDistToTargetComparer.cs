using RoamlerLocation.Api.Models;

namespace RoamlerLocation.Api.Location;

public class LocationDistToTargetComparer : IComparer<Models.Location>
{
    private readonly AnonymousLocation _targetLocation;

    public LocationDistToTargetComparer(AnonymousLocation targetLocation)
    {
        _targetLocation = targetLocation;
    }

    public int Compare(Models.Location? value, Models.Location? other)
    {
        if (value == null)
        {
            return other == null ? 0 : 1;
        }

        if (other == null)
        {
            return -1;
        }

        var valueDistToTarget = value.CalculateDistance(_targetLocation);
        var otherDistToTarget = other.CalculateDistance(_targetLocation);
        return valueDistToTarget.CompareTo(otherDistToTarget);
    }
}