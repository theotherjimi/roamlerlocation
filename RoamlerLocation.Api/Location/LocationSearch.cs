﻿using RoamlerLocation.Api.Models;
using RoamlerLocation.Api.Repositories;

namespace RoamlerLocation.Api.Location;

public class LocationSearch
{
    private readonly ILocationRepository _locationRepository;

    public LocationSearch(ILocationRepository locationRepository)
    {
        _locationRepository = locationRepository;
    }

    public IEnumerable<LocationResult> Search(AnonymousLocation targetLocation, int maxDistance, int maxResults)
    {
        return _locationRepository.GetLocationData()
            .Where(x => x.CalculateDistance(targetLocation) < maxDistance) // filtering the search space speeds things up significantly
            .OrderBy(x => x, new LocationDistToTargetComparer(targetLocation))
            .Take(maxResults)
            .Select(x => new LocationResult(x.Address, x.Latitude, x.Longitude, x.CalculateDistance(targetLocation)));
    }
}