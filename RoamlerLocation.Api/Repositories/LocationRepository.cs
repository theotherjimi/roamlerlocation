using RoamlerLocation.Api.Data;

namespace RoamlerLocation.Api.Repositories;

public class LocationRepository : ILocationRepository
{
    // cache the parsed locations so we don't have to read the CSV file more than once
    private List<Models.Location>? _locationData;

    public IEnumerable<Models.Location> GetLocationData()
    {
        return _locationData ??= CsvLoader.LoadData("locations.csv").ToList();
    }
}