﻿namespace RoamlerLocation.Api.Repositories;

public interface ILocationRepository
{
    IEnumerable<Models.Location> GetLocationData();
}