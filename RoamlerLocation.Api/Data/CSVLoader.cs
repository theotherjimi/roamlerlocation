﻿using System.Globalization;
using Csv;

namespace RoamlerLocation.Api.Data;

public static class CsvLoader
{
    // Invalid CSV data: "Kasteel 'Ru�ne Strijen""""; Hoofseweg, Oosterhout,","51.6505494","4.8605965"
    public static IEnumerable<Models.Location> LoadData(string filePath)
    {
        var format = new NumberFormatInfo
        {
            NumberDecimalSeparator = "."
        };
        using var fileStream = File.OpenRead(filePath);
        var csv = CsvReader.ReadFromStream(fileStream);
        foreach (var row in csv)
        {
            yield return new Models.Location(row["Address"],
                double.Parse(row["Latitude"], format),
                double.Parse(row["Longitude"], format));
        }
    }
}